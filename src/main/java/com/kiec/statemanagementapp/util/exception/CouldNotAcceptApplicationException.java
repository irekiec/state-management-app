package com.kiec.statemanagementapp.util.exception;

public class CouldNotAcceptApplicationException extends RuntimeException {
    public CouldNotAcceptApplicationException() {
        super("Could not accept the application as it hasn't been verified.");
    }
}
