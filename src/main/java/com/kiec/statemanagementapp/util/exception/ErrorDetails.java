package com.kiec.statemanagementapp.util.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Getter
@AllArgsConstructor
public class ErrorDetails {
    private final String message;
    private final HttpStatus httpStatus;
    private final LocalDateTime timestamp;
    private final Throwable throwable;
}
