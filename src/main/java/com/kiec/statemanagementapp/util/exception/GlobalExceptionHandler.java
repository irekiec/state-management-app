package com.kiec.statemanagementapp.util.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(value = {ApplicationNotFoundException.class})
    public ResponseEntity<?> handleApplicationNotFoundException(ApplicationNotFoundException exception) {
        return handleCommonException(exception, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {
            CouldNotVerifyApplicationException.class,
            CouldNotAcceptApplicationException.class,
            CouldNotDeleteApplication.class,
            CouldNotEditApplicationContentsException.class,
            CouldNotPublishApplicationException.class})
    public ResponseEntity<?> handleForbiddenException(RuntimeException exception) {
        return handleCommonException(exception, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    public ResponseEntity<?> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        return handleCommonException(ex, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {NullPointerException.class})
    public ResponseEntity<?> handleNullPointerException(NullPointerException ex) {
        return handleCommonException(ex, HttpStatus.BAD_REQUEST);
    }

    private ResponseEntity<?> handleCommonException(Throwable ex, HttpStatus httpStatus) {
        String message = ex.getMessage();
        log.error(message, ex);
        ErrorDetails errorDetails = new ErrorDetails(message, httpStatus, LocalDateTime.now(), ex);
        return new ResponseEntity(errorDetails, httpStatus);
    }
}
