package com.kiec.statemanagementapp.util.exception;

public class ApplicationNotFoundException extends RuntimeException {
    public ApplicationNotFoundException() {
        super("Application not found!");
    }
}
