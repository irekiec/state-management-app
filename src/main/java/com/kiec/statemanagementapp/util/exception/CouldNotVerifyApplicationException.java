package com.kiec.statemanagementapp.util.exception;

public class CouldNotVerifyApplicationException extends RuntimeException {
    public CouldNotVerifyApplicationException() {
        super("Could not verify the application as it has no CREATED state.");
    }
}
