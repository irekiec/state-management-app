package com.kiec.statemanagementapp.util.exception;

public class CouldNotDeleteApplication extends RuntimeException {
    public CouldNotDeleteApplication() {
        super("Could not delete or reject the application as it has been already published.");
    }
}
