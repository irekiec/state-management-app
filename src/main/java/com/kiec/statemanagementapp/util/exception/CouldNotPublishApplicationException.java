package com.kiec.statemanagementapp.util.exception;

public class CouldNotPublishApplicationException extends RuntimeException {
    public CouldNotPublishApplicationException() {
        super("Could not publish the application as it hasn't been accepted.");
    }
}
