package com.kiec.statemanagementapp.util.exception;

public class CouldNotEditApplicationContentsException extends RuntimeException {
    public CouldNotEditApplicationContentsException() {
        super("Could not edit application contents as the state is no longer CREATED or VERIFIED.");
    }
}
