package com.kiec.statemanagementapp.service;

import com.kiec.statemanagementapp.dto.Application;
import com.kiec.statemanagementapp.dto.ApplicationApi;
import com.kiec.statemanagementapp.dto.ApplicationChange;
import com.kiec.statemanagementapp.dto.State;
import com.kiec.statemanagementapp.repository.ApplicationChangeRepository;
import com.kiec.statemanagementapp.repository.ApplicationRepository;
import com.kiec.statemanagementapp.util.exception.ApplicationNotFoundException;
import com.sun.istack.NotNull;
import com.sun.istack.Nullable;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import static com.kiec.statemanagementapp.Constants.*;

@Service
@Slf4j
public class ApplicationService {

    private final ApplicationRepository applicationRepository;
    private final ApplicationChangeRepository applicationChangeRepository;

    @Autowired
    public ApplicationService(ApplicationRepository applicationRepository, ApplicationChangeRepository applicationChangeRepository) {
        this.applicationRepository = applicationRepository;
        this.applicationChangeRepository = applicationChangeRepository;
    }

    public void create(final ApplicationApi api) {
        Application application = new Application(api);
        applicationRepository.save(application);
        saveChange(application);
    }

    public Application getOne(final Long id) {
        return applicationRepository.findById(id).orElseThrow(ApplicationNotFoundException::new);
    }

    public Page<Application> index(@NotNull final int page,
                                   @NotNull final int size,
                                   @NotNull final String sortBy,
                                   @NotNull final Sort.Direction order,
                                   @Nullable final String title,
                                   @Nullable State state) {
        Sort sort = order.equals(Sort.Direction.ASC) ? Sort.by(sortBy).ascending() : Sort.by(sortBy).descending();
        Pageable pageable = PageRequest.of(page, size, sort);
        if (state != null)
            return applicationRepository.findAllByTitleLikeAndState("%" + title + "%", state, pageable);
        else
            return applicationRepository.findAllByTitleLike("%" + title + "%", pageable);
    }

    public void editContents(final Long id, final String contents) {
        Application application = getOne(id);
        application.editContents(contents);
        applicationRepository.save(application);
        saveChange(application);
    }

    public void rejectApplication(final Long id, final String rejectionReason) {
        Application application = getOne(id);
        application.delete(rejectionReason);
        applicationRepository.save(application);
        saveChange(application);
    }

    private void saveChange(Application application) {
        applicationChangeRepository.save(new ApplicationChange(application));
        log.info(APPLICATION_CHANGE_HAS_BEEN_SAVED);
    }

    public void verify(final Long id) {
        Application application = getOne(id);
        application.verify();
        applicationRepository.save(application);
        log.info(APPLICATION_HAS_BEEN_VERIFIED);
        saveChange(application);
    }

    public void accept(final Long id) {
        Application application = getOne(id);
        application.accept();
        applicationRepository.save(application);
        log.info(APPLICATION_HAS_BEEN_ACCEPTED);
        saveChange(application);
    }

    public void publish(final Long id) {
        Application application = getOne(id);
        application.publish();
        applicationRepository.save(application);
        log.info(APPLICATION_HAS_BEEN_PUBLISHED);
        saveChange(application);
    }
}
