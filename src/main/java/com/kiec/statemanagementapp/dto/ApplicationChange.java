package com.kiec.statemanagementapp.dto;

import com.sun.istack.NotNull;
import lombok.*;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "application_changes")
@Getter
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class ApplicationChange extends ApplicationDetails implements Serializable {
    @EmbeddedId
    @NotNull
    private ApplicationChangeId applicationChangeId;

    public ApplicationChange(Application application) {
        this.applicationChangeId = new ApplicationChangeId(application, LocalDateTime.now());
        setState(application.getState());
        setContents(application.getContents());
        setRejectionReason(application.getRejectionReason());
        setPublicationNumber(application.getPublicationNumber());
    }
}
