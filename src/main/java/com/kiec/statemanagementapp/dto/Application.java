package com.kiec.statemanagementapp.dto;

import com.kiec.statemanagementapp.util.exception.*;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

import static com.kiec.statemanagementapp.dto.State.*;

@Entity
@Table(name = "applications")
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Application extends ApplicationDetails implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String title;

    @OneToMany(
            targetEntity = ApplicationChange.class,
            mappedBy = "applicationChangeId.application",
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER)
    private List<ApplicationChange> history;

    public Application(ApplicationApi api) {
        super(api.getContents(), CREATED);
        this.title = api.getTitle();
    }

    public void editContents(String contents) {
        switch (getState()) {
            case CREATED:
            case VERIFIED:
                setContents(contents);
                break;
            default:
                throw new CouldNotEditApplicationContentsException();
        }
    }

    public void delete(String rejectionReason) {
        switch (getState()) {
            case PUBLISHED:
            case REJECTED:
            case DELETED:
                throw new CouldNotDeleteApplication();
            default:
                rejectApplication(rejectionReason);
                break;
        }
    }

    private void rejectApplication(String rejectionReason) {
        State state = CREATED.equals(getState()) ? DELETED : REJECTED;
        setState(state);
        setRejectionReason(rejectionReason);
    }

    public void verify() {
        changeStatus(CREATED, VERIFIED, new CouldNotVerifyApplicationException());
    }

    public void accept() {
        changeStatus(VERIFIED, ACCEPTED, new CouldNotAcceptApplicationException());
    }

    public void publish() {
        setPublicationNumber(id);
        changeStatus(ACCEPTED, PUBLISHED, new CouldNotPublishApplicationException());
    }

    private void changeStatus(State previousState, State currentState, RuntimeException exception) {
        if (previousState.equals(getState())) setState(currentState);
        else throw exception;
    }
}
