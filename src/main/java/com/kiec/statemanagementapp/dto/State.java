package com.kiec.statemanagementapp.dto;

public enum State {
    CREATED,
    DELETED,
    VERIFIED,
    REJECTED,
    ACCEPTED,
    PUBLISHED
}
