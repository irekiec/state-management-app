package com.kiec.statemanagementapp.dto;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@MappedSuperclass
public abstract class ApplicationDetails implements Serializable {

    @Setter
    @NotNull
    @Enumerated(EnumType.STRING)
    private State state;

    @NotNull
    private String contents;

    @Setter
    @Column(name = "rejection_reason")
    private String rejectionReason;

    @Column(name = "publication_number", unique = true)
    private Long publicationNumber;

    public ApplicationDetails(String contents, State state) {
        this.contents = contents;
        this.state = state;
    }
}
