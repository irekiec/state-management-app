package com.kiec.statemanagementapp.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class ApplicationChangeId implements Serializable {
    @ManyToOne
    @JsonIgnore
    @JoinColumn(name = "application_id", nullable = false)
    private Application application;

    @Column(name = "change_date", nullable = false)
    private LocalDateTime changeDate;
}
