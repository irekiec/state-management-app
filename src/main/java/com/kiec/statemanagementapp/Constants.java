package com.kiec.statemanagementapp;

public class Constants {
    public static final String BASE_PATH = "/api";
    public static final String APPLICATION_PATH = BASE_PATH + "/applications";

    public static final String APPLICATION_CHANGE_HAS_BEEN_SAVED = "Change of application has been saved in the history.";
    public static final String APPLICATION_HAS_BEEN_VERIFIED = "Application has been verified.";
    public static final String APPLICATION_HAS_BEEN_ACCEPTED = "Application has been accepted.";
    public static final String APPLICATION_HAS_BEEN_PUBLISHED = "Application has been published.";

    private Constants() {
    }
}
