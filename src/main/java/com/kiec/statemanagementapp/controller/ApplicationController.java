package com.kiec.statemanagementapp.controller;

import com.kiec.statemanagementapp.Constants;
import com.kiec.statemanagementapp.dto.Application;
import com.kiec.statemanagementapp.dto.ApplicationApi;
import com.kiec.statemanagementapp.dto.State;
import com.kiec.statemanagementapp.service.ApplicationService;
import com.sun.istack.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@RestController
@RequestMapping(Constants.APPLICATION_PATH)
public class ApplicationController {

    private final ApplicationService service;

    @Autowired
    public ApplicationController(ApplicationService service) {
        this.service = service;
    }

    @GetMapping
    public ResponseEntity<Page<Application>> index(
            @Valid @RequestParam(required = false, defaultValue = "0") final int page,
            @Valid @RequestParam(required = false, defaultValue = "10") final int size,
            @Valid @RequestParam(required = false, defaultValue = "ASC") final Sort.Direction order,
            @Valid @RequestParam(required = false, defaultValue = "title") final String sortBy,
            @Valid @RequestParam(required = false, defaultValue = "") final String title,
            @Nullable @RequestParam(required = false) final State state
    ) {
        return ResponseEntity.ok(service.index(page, size, sortBy, order, title, state));
    }

    @GetMapping("/{id}")
    public ResponseEntity<Application> getOne(@NotNull @PathVariable final Long id) {
        return ResponseEntity.ok(service.getOne(id));
    }

    @PostMapping
    public ResponseEntity<Void> create(@Valid @RequestBody final ApplicationApi api) {
        service.create(api);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PatchMapping("/{id}/edit")
    public ResponseEntity<Void> editContents(@NotNull @PathVariable final Long id, @NotBlank @RequestBody final String contents) {
        service.editContents(id, contents);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PatchMapping("/{id}/reject")
    public ResponseEntity<Void> rejectApplication(@NotNull @PathVariable final Long id, @NotBlank @RequestBody final String rejectionReason) {
        service.rejectApplication(id, rejectionReason);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping("/{id}/verify")
    public ResponseEntity<Void> verify(@NotNull @PathVariable final Long id) {
        service.verify(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping("/{id}/accept")
    public ResponseEntity<Void> accept(@NotNull @PathVariable final Long id) {
        service.accept(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping("/{id}/publish")
    public ResponseEntity<Void> publish(@NotNull @PathVariable final Long id) {
        service.publish(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}
