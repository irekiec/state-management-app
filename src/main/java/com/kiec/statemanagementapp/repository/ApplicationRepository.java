package com.kiec.statemanagementapp.repository;

import com.kiec.statemanagementapp.dto.Application;
import com.kiec.statemanagementapp.dto.State;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationRepository extends JpaRepository<Application, Long> {
    @Override
    Page<Application> findAll(Pageable pageable);

    Page<Application> findAllByTitleLike(String title, Pageable pageable);

    Page<Application> findAllByTitleLikeAndState(String title, State state, Pageable pageable);
}
