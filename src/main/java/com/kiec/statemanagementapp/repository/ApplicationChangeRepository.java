package com.kiec.statemanagementapp.repository;

import com.kiec.statemanagementapp.dto.ApplicationChange;
import com.kiec.statemanagementapp.dto.ApplicationChangeId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApplicationChangeRepository extends JpaRepository<ApplicationChange, ApplicationChangeId> {
}
