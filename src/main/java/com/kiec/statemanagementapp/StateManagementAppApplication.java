package com.kiec.statemanagementapp;

import com.kiec.statemanagementapp.dto.Application;
import com.kiec.statemanagementapp.dto.ApplicationApi;
import com.kiec.statemanagementapp.dto.ApplicationChange;
import com.kiec.statemanagementapp.repository.ApplicationChangeRepository;
import com.kiec.statemanagementapp.repository.ApplicationRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.List;

@SpringBootApplication
public class StateManagementAppApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(StateManagementAppApplication.class, args);
        ApplicationRepository applicationRepository = context.getBean(ApplicationRepository.class);
        ApplicationChangeRepository applicationChangeRepository = context.getBean(ApplicationChangeRepository.class);

        Application application1 = new Application(new ApplicationApi("app1", "djfksdfj dfhksdhf"));
        Application application2 = new Application(new ApplicationApi("app2", "djfksdfj dfhksdhf"));
        Application application3 = new Application(new ApplicationApi("app3", "djfksdfj dfhksdhf"));
        Application application4 = new Application(new ApplicationApi("app3", "djfksdfj dfhksdhf"));
        applicationRepository.saveAll(List.of(application1, application2, application3, application4));

        applicationChangeRepository.saveAll(List.of(new ApplicationChange(application1),
                new ApplicationChange(application2),
                new ApplicationChange(application3),
                new ApplicationChange(application4)));
    }

}
