package com.kiec.statemanagementapp;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import javax.transaction.Transactional;

@SpringBootTest
@Transactional
@ActiveProfiles("local")
class StateManagementAppApplicationTests {

    @Test
    void contextLoads() {
    }

}
