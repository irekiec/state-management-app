package com.kiec.statemanagementapp;

import com.kiec.statemanagementapp.dto.Application;
import com.kiec.statemanagementapp.dto.ApplicationApi;
import com.kiec.statemanagementapp.dto.State;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Sort;

import java.util.List;

public class AbstractTest {
    protected static final String TITLE = "title";
    protected static final String CONSENTS = "contents";
    protected static final String CONSENTS_2 = "contents2";
    protected static final String REJECTION_REASON = "rejectReason";
    protected static final Long ID = 9999L;
    protected static final int PAGE = 0;
    protected static final int SIZE = 10;
    protected static final String SORT_BY = "title";
    protected static final Sort.Direction ORDER = Sort.Direction.ASC;
    protected static final String BASE_PATH = "/api/applications";

    @Captor
    protected ArgumentCaptor<Application> appCaptor;
    @Captor
    protected ArgumentCaptor<ApplicationApi> appApiCaptor;


    protected Page<Application> createApplicationPage() {
        return new PageImpl<>(List.of(createApplication()));
    }

    protected Application createApplication() {
        return new Application(createApplicationApi());
    }

    protected Application createApplication(State state) {
        Application application = createApplication();
        application.setState(state);
        return application;
    }

    protected ApplicationApi createApplicationApi() {
        return new ApplicationApi(TITLE, CONSENTS);
    }

    protected String jsonCreateApi() {
        return "{\"" + TITLE + "\":\"" + TITLE + "\",\"" + CONSENTS + "\":\"" + CONSENTS + "\"}";
    }
}
