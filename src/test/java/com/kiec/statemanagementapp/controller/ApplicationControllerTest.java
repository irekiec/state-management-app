package com.kiec.statemanagementapp.controller;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kiec.statemanagementapp.AbstractTest;
import com.kiec.statemanagementapp.dto.ApplicationApi;
import com.kiec.statemanagementapp.service.ApplicationService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import javax.transaction.Transactional;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@ActiveProfiles("local")
class ApplicationControllerTest extends AbstractTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    ApplicationService service;

    ObjectMapper mapper;

    @BeforeEach
    public void setUp() {
        mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    @Test
    void index() throws Exception {
        when(service.index(anyInt(), anyInt(), anyString(), any(), any(), any()))
                .thenReturn(createApplicationPage());

        mockMvc.perform(get(BASE_PATH))
                .andExpect(status().isOk());

        Mockito.verify(service).index(PAGE, SIZE, SORT_BY, ORDER, "", null);
    }

    @Test
    void getOne() throws Exception {
        when(service.getOne(anyLong()))
                .thenReturn(createApplication());

        mockMvc.perform(get(BASE_PATH + "/" + ID))
                .andExpect(status().isOk());

        Mockito.verify(service).getOne(ID);
    }

    @Test
    void create() throws Exception {
        mockMvc.perform(post(BASE_PATH)
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonCreateApi()))
                .andExpect(status().isCreated());

        Mockito.verify(service).create(appApiCaptor.capture());
        ApplicationApi appApi = appApiCaptor.getValue();
        Assertions.assertThat(appApi.getTitle()).isEqualTo(TITLE);
        Assertions.assertThat(appApi.getContents()).isEqualTo(CONSENTS);
    }

    @Test
    void editContents() throws Exception {
        mockMvc.perform(patch(BASE_PATH + "/" + ID + "/edit")
                .content(CONSENTS))
                .andExpect(status().isOk());

        Mockito.verify(service).editContents(ID, CONSENTS);
    }

    @Test
    void rejectApplication() throws Exception {
        mockMvc.perform(patch(BASE_PATH + "/" + ID + "/reject")
                .content(REJECTION_REASON))
                .andExpect(status().isOk());

        Mockito.verify(service).rejectApplication(ID, REJECTION_REASON);
    }

    @Test
    void verify() throws Exception {
        mockMvc.perform(get(BASE_PATH + "/" + ID + "/verify"))
                .andExpect(status().isOk());

        Mockito.verify(service).verify(ID);
    }

    @Test
    void accept() throws Exception {
        mockMvc.perform(get(BASE_PATH + "/" + ID + "/accept"))
                .andExpect(status().isOk());

        Mockito.verify(service).accept(ID);
    }

    @Test
    void publish() throws Exception {
        mockMvc.perform(get(BASE_PATH + "/" + ID + "/publish"))
                .andExpect(status().isOk());

        Mockito.verify(service).publish(ID);
    }
}