package com.kiec.statemanagementapp.service;

import com.kiec.statemanagementapp.AbstractTest;
import com.kiec.statemanagementapp.dto.Application;
import com.kiec.statemanagementapp.dto.ApplicationApi;
import com.kiec.statemanagementapp.dto.ApplicationChange;
import com.kiec.statemanagementapp.dto.State;
import com.kiec.statemanagementapp.repository.ApplicationChangeRepository;
import com.kiec.statemanagementapp.repository.ApplicationRepository;
import com.kiec.statemanagementapp.util.exception.*;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;

import static com.kiec.statemanagementapp.dto.State.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ApplicationServiceTest extends AbstractTest {

    @Mock
    ApplicationRepository applicationRepository;

    @Mock
    ApplicationChangeRepository applicationChangeRepository;

    @InjectMocks
    ApplicationService service;

    @Test
    void shouldCreateApplication() {
        whenSave();
        ApplicationApi api = createApplicationApi();

        service.create(api);

        Mockito.verify(applicationRepository).save(appCaptor.capture());
        Application app = appCaptor.getValue();
        assertThat(app.getTitle()).isEqualTo(api.getTitle());
        assertThat(app.getContents()).isEqualTo(api.getContents());
    }

    @Test
    void shouldGetOneApplication() {
        whenGetOneReturnsApplication();

        Application application = service.getOne(ID);

        assertThat(application.getTitle()).isEqualTo(TITLE);
        assertThat(application.getContents()).isEqualTo(CONSENTS);
        assertThat(application.getState()).isEqualTo(CREATED);
    }

    @Test
    void shouldThrowException_whenTryingGetNotExistingApplication() {
        whenGetOneReturnsNull();

        assertThrows(ApplicationNotFoundException.class, () -> service.getOne(ID));
    }

    @Test
    void shouldIndexApplications_whenStateIsNull() {
        Page<Application> applicationPage = createApplicationPage();
        when(applicationRepository.findAllByTitleLike(anyString(), any()))
                .thenReturn(applicationPage);

        Page<Application> index = service.index(PAGE, SIZE, SORT_BY, ORDER, null, null);

        assertCommonIndex(index);
    }

    @Test
    void shouldIndexApplications_whenStateIsNotNull() {
        Page<Application> applicationPage = createApplicationPage();
        when(applicationRepository.findAllByTitleLikeAndState(anyString(), any(), any()))
                .thenReturn(applicationPage);

        Page<Application> index = service.index(PAGE, SIZE, SORT_BY, ORDER, null, CREATED);

        assertCommonIndex(index);
    }

    @Test
    void shouldThrowNullPointer_whenIndexWithNullParameters() {
        assertThrows(NullPointerException.class,
                () -> service.index(0, 0, null, null, null, null));
    }

    @Test
    void shouldEditContents_whenStateIsCreated() {
        whenSave();
        whenGetOneReturnsApplication();

        service.editContents(ID, CONSENTS_2);

        assertCommonEditConsent();
    }

    @Test
    void shouldEditContents_whenStateIsVerified() {
        whenSave();
        whenGetOneReturnsApplication(VERIFIED);

        service.editContents(ID, CONSENTS_2);

        assertCommonEditConsent();
    }

    @Test
    void shouldThrowException_whenTryingEditConsentsAndStateIsAccepted() {
        whenGetOneReturnsApplication(ACCEPTED);

        assertThrows(CouldNotEditApplicationContentsException.class, () -> service.editContents(ID, CONSENTS_2));
    }

    @Test
    void shouldThrowException_whenTryingEditConsentsAndStateIsPublished() {
        whenGetOneReturnsApplication(PUBLISHED);

        assertThrows(CouldNotEditApplicationContentsException.class, () -> service.editContents(ID, CONSENTS_2));
    }

    @Test
    void shouldThrowException_whenTryingEditConsentsAndStateIsRejected() {
        whenGetOneReturnsApplication(REJECTED);

        assertThrows(CouldNotEditApplicationContentsException.class, () -> service.editContents(ID, CONSENTS_2));
    }

    @Test
    void shouldThrowException_whenTryingEditConsentsAndStateIsDeleted() {
        whenGetOneReturnsApplication(DELETED);

        assertThrows(CouldNotEditApplicationContentsException.class, () -> service.editContents(ID, CONSENTS_2));
    }

    @Test
    void shouldRejectApplication_whenStateIsCreated() {
        whenSave();
        whenGetOneReturnsApplication();

        service.rejectApplication(ID, REJECTION_REASON);

        assertCommonRejectApplication(DELETED);
    }

    @Test
    void shouldRejectApplication_whenStateIsVerified() {
        whenSave();
        whenGetOneReturnsApplication(VERIFIED);

        service.rejectApplication(ID, REJECTION_REASON);

        assertCommonRejectApplication(REJECTED);
    }

    @Test
    void shouldRejectApplication_whenStateIsAccepted() {
        whenSave();
        whenGetOneReturnsApplication(VERIFIED);

        service.rejectApplication(ID, REJECTION_REASON);

        assertCommonRejectApplication(REJECTED);
    }

    @Test
    void shouldThrowException_whenTryingToRejectAndStateIsPublished() {
        whenGetOneReturnsApplication(PUBLISHED);

        assertThrows(CouldNotDeleteApplication.class, () -> service.rejectApplication(ID, REJECTION_REASON));
    }

    @Test
    void shouldThrowException_whenTryingToRejectAndStateIsRejected() {
        whenGetOneReturnsApplication(REJECTED);

        assertThrows(CouldNotDeleteApplication.class, () -> service.rejectApplication(ID, REJECTION_REASON));
    }

    @Test
    void shouldThrowException_whenTryingToRejectAndStateIsDeleted() {
        whenGetOneReturnsApplication(DELETED);

        assertThrows(CouldNotDeleteApplication.class, () -> service.rejectApplication(ID, REJECTION_REASON));
    }

    @Test
    void shouldVerifyApplication_whenStatIsCreated() {
        whenSave();
        whenGetOneReturnsApplication();

        service.verify(ID);

        Mockito.verify(applicationRepository).save(appCaptor.capture());
        Application app = appCaptor.getValue();
        assertThat(app.getState()).isNotNull().isEqualTo(VERIFIED);
    }

    @Test
    void shouldThrowException_whenTryingVerifyAndStateIsNotCreated() {
        whenGetOneReturnsApplication(ACCEPTED);

        assertThrows(CouldNotVerifyApplicationException.class, () -> service.verify(ID));
    }

    @Test
    void shouldAccept_whenStateIsVerified() {
        whenSave();
        whenGetOneReturnsApplication(VERIFIED);

        service.accept(ID);

        Mockito.verify(applicationRepository).save(appCaptor.capture());
        Application app = appCaptor.getValue();
        assertThat(app.getState()).isNotNull().isEqualTo(ACCEPTED);
    }

    @Test
    void shouldThrowsException_whenTryingAcceptAndStateIsNotVerified() {
        whenGetOneReturnsApplication(ACCEPTED);

        assertThrows(CouldNotAcceptApplicationException.class, () -> service.accept(ID));
    }

    @Test
    void shouldPublish_whenStateIsAccepted() {
        whenSave();
        whenGetOneReturnsApplication(ACCEPTED);

        service.accept(ID);

        Mockito.verify(applicationRepository).save(appCaptor.capture());
        Application app = appCaptor.getValue();
        assertThat(app.getState()).isNotNull().isEqualTo(PUBLISHED);
    }

    @Test
    void shouldThrowsException_whenTryingPublishAndStateIsNotAccepted() {
        whenGetOneReturnsApplication(VERIFIED);

        assertThrows(CouldNotPublishApplicationException.class, () -> service.publish(ID));
    }

    private void whenGetOneReturnsApplication() {
        when(applicationRepository.findById(anyLong()))
                .thenReturn(Optional.of(createApplication()));
    }

    private void whenGetOneReturnsApplication(State state) {
        when(applicationRepository.findById(anyLong()))
                .thenReturn(Optional.of(createApplication(state)));
    }

    private void assertCommonIndex(Page<Application> index) {
        List<Application> content = index.getContent();
        Application application = content.get(0);
        assertThat(content.size()).isEqualTo(1);
        assertThat(application).isNotNull();
        assertThat(application.getTitle()).isEqualTo(TITLE);
        assertThat(application.getContents()).isEqualTo(CONSENTS);
        assertThat(application.getState()).isEqualTo(CREATED);
    }

    private void whenSave() {
        when(applicationRepository.save(any())).thenReturn(new Application());
        when(applicationChangeRepository.save(any())).thenReturn(new ApplicationChange());
    }

    private void whenGetOneReturnsNull() {
        when(applicationRepository.findById(anyLong())).thenReturn(Optional.empty());
    }

    private void assertCommonEditConsent() {
        Mockito.verify(applicationRepository).save(appCaptor.capture());
        Application app = appCaptor.getValue();
        assertThat(app.getContents()).isEqualTo(CONSENTS_2);
    }

    private void assertCommonRejectApplication(State currentState) {
        Mockito.verify(applicationRepository).save(appCaptor.capture());
        Application app = appCaptor.getValue();
        assertThat(app.getRejectionReason()).isNotNull().isEqualTo(REJECTION_REASON);
        assertThat(app.getState()).isEqualTo(currentState);
    }
}